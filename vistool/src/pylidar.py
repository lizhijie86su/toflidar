#! /usr/bin/python3

## Author: Ryan Brazeal
## Email: ryan.brazeal@ufl.edu
## Date Started: Nov 24th, 2018
## Description: Python3 based driver for SlamTec RPLidar 

## Notes:
##
##    Nov.28th, 2018 - Received an email reply from SlamTec support and apparently new protocol documentation is currently being written to illustrate the new A3 scan rates
##
##    Dec.2nd, 2018 - Standard Scan Rate (~4 KHz) is working properly
##
##    Dec.10th, 2018 - Express Scan Rate (~8 KHz) is working properly
##                     Due to SlamTec protocol document not being updated for A3 device (and I don't feel like reverse engineering the C++ code), no other scan rates are currently working

## RPyLIDAR Viewer Instructions:
##
##    Mouse: 
##        Left Click and Hold - moves (translates) the scan data on the screen
##        Right Click and Hold -  rotates the scan data on the screen (display purposes only, i.e., recorded scan data is unaffected)
##        Mouse Wheel - forward to zoom in, backwards to zoom out
##        Double Left Click - re-center the scan data
##        Double Right Click - re-orientate (rotate) the scan data
##        Double Middle Click - reset the zoom level
##
##    Keyboard:
##        r key    - toogles the faint ray traces on/off within the viewer
##        [/{ key - increases point size
##        ]/} key - decreases point size
##        =/+ key - zooms in
##        -/_ key - zooms out
##        Up/Down/Left/Right keys - moves the scan data in the respective direction
##        ,/< key    - rotates the scan data counter-clockwise
##        ./> key - rotates the scan data clockwise
##        c key    - re-center the scan data
##        o key    - re-orientate (rotate) the scan data
##        z key    - reset the zoom level
##        i key    - re-center and re-orientate and reset the zoom (initializes the default viewer settings)
##        Esc key    - Ends the scanning session and closes the viewer


# Standard Imports
import os
import time
import math
import binascii
import threading

#Non-standard Imports
import serial                    #install pyserial
from serial.tools import list_ports

import pygame                    #install pygame
import pygame_widgets
from pygame_widgets.textbox import TextBox
from pygame_widgets.button import Button
from pygame_widgets.dropdown import Dropdown

import numpy as np

# struct packed_tdc_data2
# {
#   uint8_t head[2];
#   uint8_t type;
#   uint8_t points;
#   uint16_t beg_angle;
#   uint16_t end_angle;
#   uint16_t speed;
#   uint16_t data[SEND_POINTS_PER_GROUP];
#   uint16_t crc;
#   uint8_t tail[2];
# }__attribute__ ((packed));

class LidarPoint(object):
    def __init__(self, distance, intencity, degree):
        self.distance = distance
        self.intencity = intencity
        self.degree = degree

    def getXY(self):
        rad = math.radians(self.degree * 1.0 / 100)
        return (self.distance * np.cos(rad), self.distance * np.sin(rad))

class FrameData(object):
    def __init__(self, index):
        self.index = index
        self.points = list()
        self.speed = 0.0
        self.zero_points = 0

class DataReceiver(object):
    FRAME_POOL_SIZE = 10
    PKG_HEAD1 = 0xFA
    PKG_HEAD2 = 0xF5
    PKG_TAIL1 = 0x31
    PKG_TAIL2 = 0xF2
    POINTS_PER_PKG = 32
    PKG_HEAD_LEN = 10
    PKG_TAIL_LEN = 4
    PKG_SIZE = PKG_HEAD_LEN + 2 * POINTS_PER_PKG + PKG_TAIL_LEN

    def __init__(self, COM):
        self.stop = False
        self.COM = COM
        self.ready_data_pool = list()
        self.th = threading.Thread(target=self.worker)
        self.th.daemon = True
        self.th.start()

    def is_valid_packet(self, data):
        return len(data) == self.PKG_SIZE and \
            data[0] == self.PKG_HEAD1 and \
            data[1] == self.PKG_HEAD2 and \
            data[-1] == self.PKG_TAIL2 and \
            data[-2] == self.PKG_TAIL1
    
    def getXY(self, distance, angle):
        rad = math.radians(angle * 1.0 / 100)
        return (distance * np.cos(rad), distance * np.sin(rad))
    
    def get_one_packet(self):
        while self.stop == False:
            try:
                data = self.COM.read(self.PKG_SIZE)
                if self.is_valid_packet(data):
                    return data
                else:
                    print("received invalid data. length = %d" % len(data))
            except Exception as e:
                print("Exception recv:", str(e))
                continue

    def start_get_packet(self):
        last_data = None
        while self.stop == False:
            data = self.COM.read()
            if len(data) == 0:
                continue
            if data[0] == self.PKG_TAIL2 and last_data == self.PKG_TAIL1:
                break
            last_data = data[0]

    def stop_and_join(self):
        self.stop = True
        self.th.join()

    def worker(self):
        frame_counter = 0
        print("start serial receving worker.", flush=True)
        self.start_get_packet()
        print("start ...")
        while self.stop == False:
            frame = FrameData(frame_counter)
            last_degree = 0.0
            while self.stop == False:
                packet = self.get_one_packet()
                begin_angle = (int(packet[5]) << 8) | packet[4]
                end_angle = (int(packet[7]) << 8) | packet[6]
                speed = (int(packet[9]) << 8) | packet[8]
                delta_angle = ((end_angle + 36000 - begin_angle) % 36000) / self.POINTS_PER_PKG
                for i in range(self.POINTS_PER_PKG):
                    distance = (int(packet[self.PKG_HEAD_LEN + i * 2 + 1]) << 8) | packet[self.PKG_HEAD_LEN + i * 2 + 0]
                    # intencity = (int(packet[self.PKG_HEAD_LEN + i * 6 + 3]) << 8) | packet[self.PKG_HEAD_LEN + i * 6 + 2]
                    # degree = (int(packet[self.PKG_HEAD_LEN + i * 6 + 5]) << 8) | packet[self.PKG_HEAD_LEN + i * 6 + 4]
                    degree = (begin_angle + i * delta_angle) % 36000
                    if degree < last_degree:
                        # print("frame %d is ready. points = %d" % (frame_counter, len(frame.points)), flush=True)
                        frame.speed = speed
                        self.ready_data_pool.append(frame)
                        frame = FrameData(frame_counter)
                    else:
                        # print("new point: D: %d, I: 0x%x, degree: %d" % (distance, intencity, degree), flush=True)
                        if distance > 0:
                            frame.points.append(self.getXY(distance, degree))
                        else:
                            frame.zero_points += 1
                    last_degree = degree

                frame_counter += 1
                # print("get one frame: %d" % frame_counter, flush=True)
                if len(self.ready_data_pool) > self.FRAME_POOL_SIZE:
                    print("pop one frame since frame pool is full. size = %s" % (len(self.ready_data_pool)))
                    self.ready_data_pool.pop(0)
        print("stop serial receving worker.", flush=True)

    def get(self):
        return self.ready_data_pool.pop(0) if len(self.ready_data_pool) > 0 else None

#lidar sensor device class
class RPyLIDAR:
    
    #default constructor
    def __init__(self, screenSize, drawRaysDefault, DEBUG):
        #sensor/scanning parameters
        self.pulseRateStandard = 0
        self.pulseRateExpress = 0
        self.rotationRate = 0
        self.new_points = []
        self.zeroPts = 0
        self.startTime = 0
        self.scanCount = 0
        self.totalNumPts = 0
        self.debugMode = DEBUG
                
        #pygame parameters        
        self.screen = None
        self.screenSize = screenSize
        self.drawScale = 0.001 * screenSize
        self.centerX = 0
        self.centerY = 0
        self.pointSize = 2
        self.myfont = None
        self.drawRays = drawRaysDefault
        self.drawRaysDefault = drawRaysDefault
        self.aziCorr = 0.0
        self.csXColour = [255,0,0]
        self.csYColour = [0,255,0]
        self.rayColour = [40,40,40]
        self.pointColour = [0,0,255]
        self.sensorColour = [0,0,0]
        self.bgColour = [20,20,20]
        self.textColour = [255,255,255]
        self.leftButton = False
        self.rightButton = False
        self.mouseClickPos = None
        self.rightClickAzi = 0
        self.lastLeftClickTime = 0
        self.lastRightClickTime = 0
        self.lastMidClickTime = 0
        self.doubleClickTime = 0.25

        self.COM = serial.Serial()
        self.COM.setDTR(False)    #required flag must be set, didn't find mention of this in the documentation
        self.COM.baudrate = 460800
        self.COM.timeout = 1
        self.is_connected = False

        self.init_screan()
        print("")
        
    def init_screan(self):
        #setup default pygame environment
        os.environ["SDL_VIDEO_CENTERED"] = "1"
        #os.environ["SDL_VIDEO_WINDOW_POS"] = "%d,%d" % (20,20)
        pygame.init()
        
        #self.myfont = pygame.font.Font('/usr/share/fonts/truetype/freefont/FreeSans.ttf', 14)  # LINUX
        #self.myfont = pygame.font.Font('/System/Library/Fonts/LucidaGrande.ttc', 13)  # MAC
        self.myfont = pygame.font.Font('C:\\Windows\\Fonts\\Arial.ttf', 13)   # WINDOWS
        
        pygame.display.set_caption("RPyLIDAR Viewer")
        self.screen = pygame.display.set_mode([self.screenSize, self.screenSize])
        self.screen.fill(self.bgColour)
        self.centerX = int(self.screenSize / 2)
        self.centerY = int(self.screenSize / 2)

        # widget
        widgit_margin = 2
        widgit_width = 80
        widgit_height = 30
        port_list = list(list_ports.comports())
        self.port_list_choices = [list(port)[0] for port in port_list]
        print("serialDropdown", self.port_list_choices)
        self.serialDropdown = Dropdown(
            self.screen,
            self.screenSize - (widgit_margin + widgit_width),
            widgit_margin,
            widgit_width,
            widgit_height,
            name="SerialPort",
            choices=self.port_list_choices,
            borderRadius=2,
            colour=pygame.Color('green'),
            #values=port_list_values,
            direction='down',
            #textHAlign='left'
        )

        self.connectButton = Button(
            # Mandatory Parameters
            self.screen,  # Surface to place button on
            self.screenSize - (widgit_margin + widgit_width),  # X-coordinate of top left corner
            widgit_margin * 2 + widgit_height,  # Y-coordinate of top left corner
            widgit_width,  # Width
            widgit_height,  # Height

            # Optional Parameters
            text='Connect',  # Text to display
            #fontSize=50,  # Size of font
            #margin=20,  # Minimum distance between text/image and edge of button
            inactiveColour=(200, 50, 0),  # Colour of button when not being interacted with
            hoverColour=(150, 0, 0),  # Colour of button when being hovered over
            pressedColour=(0, 200, 20),  # Colour of button when being clicked
            radius=2,  # Radius of border corners (leave empty for not curved)
            onClick=self.connect  # Function to call when clicked on
        )

        self.disconnectButton = Button(
            # Mandatory Parameters
            self.screen,  # Surface to place button on
            self.screenSize - (widgit_margin + widgit_width),  # X-coordinate of top left corner
            widgit_margin * 3 + widgit_height * 2,  # Y-coordinate of top left corner
            widgit_width,  # Width
            widgit_height,  # Height

            # Optional Parameters
            text='Disconnect',  # Text to display
            #fontSize=50,  # Size of font
            #margin=20,  # Minimum distance between text/image and edge of button
            inactiveColour=(200, 50, 0),  # Colour of button when not being interacted with
            hoverColour=(150, 0, 0),  # Colour of button when being hovered over
            pressedColour=(0, 200, 20),  # Colour of button when being clicked
            radius=2,  # Radius of border corners (leave empty for not curved)
            onClick=self.disconnect  # Function to call when clicked on
        )
        pygame_widgets.update(pygame.event.get())
        pygame.display.update()

    def connect(self):
        if self.is_connected:
            return

        self.COM.port = self.serialDropdown.getSelected()
        if self.COM.port not in self.port_list_choices:
            print("Please select a valid serial port.")
            return

        try:
            print("Connecting to " + self.COM.port + " at " + str(self.COM.baudrate) + " baud...")
            self.COM.open()
            self.is_connected = True
            time.sleep(0.1)
            self.data_receiver = DataReceiver(self.COM)
            self.startTime = time.time()
            self.lastLeftClickTime = self.startTime
            self.lastRightClickTime = self.startTime
            self.lastMidClickTime = self.startTime
        except Exception as e:
            print("Error opening serial port: " + self.COM.port)
            print(str(e))
            return

    def disconnect(self):
        if self.is_connected:
            print("Disconnecting from " + self.COM.port + "...")
            self.data_receiver.stop_and_join()
            self.data_receiver = None
            self.COM.close()
            self.is_connected = False
            time.sleep(0.1)

    #very ugly checksum generator for '\x00\x11\x22... string format', adds the checksum byte to the end of the input string
    def add_checksum(self, the_bytes):
        checksum = 0
        newCommand = ''
        
        for el in the_bytes:
            checksum ^= ord(el)
            indiv = hex(ord(el))[1:]
            if len(indiv) == 2:
                indiv = indiv[0:1] + "0" + indiv[1:]
            newCommand += '\\' + indiv

        temp = hex(checksum)[1:]
        if len(temp) == 2:
            temp = temp[0:1] + "0" + temp[1:]
        newCommand += '\\' + temp
        temp2 = str.encode(newCommand)
        newCommand = temp2.decode('unicode-escape').encode('ISO-8859-1')
        return(newCommand)

    #main pygame display/draw/update function
    def drawPoints(self):
        #found it more efficient to simply clear the entire display before redrawing features
        self.screen.fill(self.bgColour)

        #new_points list contains only the points collected from the most recent scan (i.e., single rotation of the sensor)
        for i in range(0,len(self.new_points)):
            X = int(self.new_points[i][0] * self.drawScale) + self.centerX
            Y = int(self.new_points[i][1] * self.drawScale) + self.centerY
            currentPos = (X,Y,self.pointSize,self.pointSize)
            #if the user wants to add faint ray traces to the screen
            if self.drawRays:
                pygame.draw.line(self.screen, self.rayColour, (X,Y), (self.centerX,self.centerY))
            pygame.draw.rect(self.screen, self.pointColour, currentPos, 0)
        
        #adds screen text objects to the upper right (dynamic scanning parameters) and lower right (instructions) corners of the screen
        textsurface = self.myfont.render("Rotation Rate:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,5,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,5))
        textsurface = self.myfont.render(str(self.rotationRate) + " Hz", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,5,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,5))
        
        textsurface = self.myfont.render("Points in Scan:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,20,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,20))
        textsurface = self.myfont.render(str(len(self.new_points)) + " / " + str(self.zeroPts), False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,20,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,20))
        
        textsurface = self.myfont.render("Pulses / sec:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,35,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,35))
        textsurface = self.myfont.render(str(int((len(self.new_points) + self.zeroPts) * self.rotationRate)), False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,35,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,35))
        
        textsurface = self.myfont.render("Duration:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,50,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,50))
        textsurface = self.myfont.render(str(round(time.time() - self.startTime,1) if self.is_connected else 0) + " sec", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,50,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,50))
        
        textsurface = self.myfont.render("Total Scans:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,65,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,65))
        textsurface = self.myfont.render(str(self.scanCount), False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,65,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,65))
        
        textsurface = self.myfont.render("Total Points:", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,80,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,80))
        textsurface = self.myfont.render(str(self.totalNumPts), False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (105,80,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(105,80))
        
        textsurface = self.myfont.render("Controls:  Mouse  +  -  [  ]  <  >  up  down  left  right  r  c  z  o  i  Esc", False, self.textColour)
        pygame.draw.rect(self.screen, self.bgColour, (5,self.screenSize - 25,textsurface.get_width()+20,textsurface.get_height()), 0)
        self.screen.blit(textsurface,(5,self.screenSize - 25))

        #draws the sensor object and coordinate system axes to the screen 
        pygame.draw.circle(self.screen, self.sensorColour, [self.centerX, self.centerY], 7, 0)
        pygame.draw.circle(self.screen, [255,255,255], [self.centerX, self.centerY], 7, 1)
        
        #coordinate system axes that don't rotate with the self.aziCorr parameters
        #line1 = pygame.draw.line(self.screen, self.csXColour, (self.centerX,self.centerY), (self.screenSize,self.centerY))
        #line2 = pygame.draw.line(self.screen, self.csYColour, (self.centerX,self.centerY), (self.centerX,0))
        
        #coordinate system axes that do rotate with the self.aziCorr parameters
        csAngle = self.aziCorr * math.pi / 180.0
        x1 = 0.0 
        y1 = 0.0
        x2 = 0.0 
        y2 = 0.0
        if int(self.aziCorr) == 0 or int(self.aziCorr) == 360:
            x1 = self.screenSize
            y1 = self.centerY
            x2 = self.centerX
            y2 = 0
        elif int(self.aziCorr) == 90:
            x1 = self.centerX 
            y1 = 0
            x2 = 0
            y2 = self.centerY
        elif int(self.aziCorr) == 180:
            x1 = 0
            y1 = self.centerY
            x2 = self.centerX 
            y2 = self.screenSize
        elif int(self.aziCorr) == 270:
            x1 = self.centerX
            y1 = self.screenSize
            x2 = self.screenSize 
            y2 = self.centerY
        elif self.aziCorr > 0.0 and self.aziCorr < 90.0:
            x1 = self.screenSize
            y1 = self.centerY - math.tan(csAngle) * (self.screenSize - self.centerX)
            x2 = self.centerX - math.tan(csAngle) * self.centerY
            y2 = 0
        elif self.aziCorr > 90.0 and self.aziCorr < 180.0:
            csAngle -= (1.5 * math.pi)
            x1 = self.centerX - math.tan(csAngle) * self.centerY
            y1 = 0
            x2 = 0
            y2 = self.centerY + math.tan(csAngle) * self.centerX
        elif self.aziCorr > 180.0 and self.aziCorr < 270:
            csAngle -= math.pi
            x1 = 0
            y1 = self.centerY + math.tan(csAngle) * self.centerX
            x2 = self.centerX + math.tan(csAngle) * (self.screenSize - self.centerY)
            y2 = self.screenSize
        elif self.aziCorr > 270 and self.aziCorr < 360:
            csAngle -= (0.5 * math.pi)
            x1 = self.centerX + math.tan(csAngle) * (self.screenSize - self.centerY)
            y1 = self.screenSize
            x2 = self.screenSize
            y2 = self.centerY - math.tan(csAngle) * (self.screenSize - self.centerX)
            
        pygame.draw.line(self.screen, self.csXColour, (self.centerX,self.centerY), (int(x1),int(y1)))
        pygame.draw.line(self.screen, self.csYColour, (self.centerX,self.centerY), (int(x2),int(y2)))
        
        #update the viewer and then reset the necessary parameters
        pygame_widgets.update(pygame.event.get())
        pygame.display.update()
        self.new_points = []
        self.zeroPts = 0
        
    def getControlInputs(self, stopper):
        funcStopper = stopper
        for events in pygame.event.get():
            if (events.type == pygame.QUIT):
                funcStopper = True
            elif (events.type == pygame.KEYDOWN):
                if (events.key == pygame.K_ESCAPE):
                    funcStopper = True
                elif (events.key == pygame.K_RIGHTBRACKET):
                    self.pointSize += 1
                    if self.pointSize > 5:
                        self.pointSize = 5
                elif (events.key == pygame.K_LEFTBRACKET):
                    self.pointSize -= 1
                    if self.pointSize < 1:
                        self.pointSize = 1
                elif (events.key == pygame.K_EQUALS):
                    self.drawScale *= 1.1
                    if self.drawScale > 500:
                        self.drawScale = 500
                elif (events.key == pygame.K_MINUS):
                    self.drawScale *= 0.9
                    if self.drawScale < 10:
                        self.drawScale = 10
                elif (events.key == pygame.K_DOWN):
                    self.centerY += 20
                    if self.centerY > self.screenSize * 1.25:
                        self.centerY = int(self.screenSize * 1.25)
                elif (events.key == pygame.K_UP):
                    self.centerY -= 20
                    if self.centerY < -0.25 * self.screenSize:
                        self.centerY = int(-0.25 * self.screenSize)    
                elif (events.key == pygame.K_RIGHT):
                    self.centerX += 20
                    if self.centerX > self.screenSize * 1.25:
                        self.centerX = int(self.screenSize * 1.25)
                elif (events.key == pygame.K_LEFT):
                    self.centerX -= 20
                    if self.centerX < -0.25 * self.screenSize:
                        self.centerX = (-0.25 * self.screenSize)
                elif (events.key == pygame.K_i):
                    self.centerX = int(self.screenSize / 2)
                    self.centerY = int(self.screenSize / 2)
                    self.drawScale = 0.08 * self.screenSize
                    self.pointSize = 2
                    self.aziCorr = 0.0
                    self.drawRays = self.drawRaysDefault
                elif (events.key == pygame.K_PERIOD):
                    self.aziCorr -= 2
                    if self.aziCorr < 0:
                        self.aziCorr += 360.0
                elif (events.key == pygame.K_COMMA):
                    self.aziCorr += 2
                    if self.aziCorr > 360:
                        self.aziCorr -= 360.0
                elif (events.key == pygame.K_z):
                    self.drawScale = 0.08 * self.screenSize
                elif (events.key == pygame.K_r):
                    self.drawRays = not self.drawRays
                elif (events.key == pygame.K_c):
                    self.centerX = int(self.screenSize / 2)
                    self.centerY = int(self.screenSize / 2)
                elif (events.key == pygame.K_o):
                    self.aziCorr = 0.0
            elif (events.type == pygame.MOUSEBUTTONDOWN):
                if (events.button == 1):
                    self.leftButton = True
                    self.mouseClickPos = pygame.mouse.get_pos()
                elif (events.button == 3):
                    self.rightButton = True
                    self.mouseClickPos = pygame.mouse.get_pos()
                    refAzi = math.atan2((self.mouseClickPos[0] - self.centerX),(self.centerY - self.mouseClickPos[1]))
                    if refAzi < 0:
                        refAzi += (2.0 * math.pi)
                    self.rightClickAzi = refAzi
                elif (events.button == 4):
                    self.drawScale *= 1.1
                    if self.drawScale > 500:
                        self.drawScale = 500
                elif (events.button == 5):
                    self.drawScale *= 0.9
                    if self.drawScale < 10:
                        self.drawScale = 10
            elif (events.type == pygame.MOUSEBUTTONUP):
                nowTime = time.time()
                if (events.button == 1):
                    self.leftButton = False
                    if nowTime - self.lastLeftClickTime < self.doubleClickTime:
                        self.centerX = int(self.screenSize / 2)
                        self.centerY = int(self.screenSize / 2)
                    self.lastLeftClickTime = nowTime
                elif (events.button == 2):
                    if nowTime - self.lastMidClickTime < self.doubleClickTime:
                        self.drawScale = 0.08 * self.screenSize
                    self.lastMidClickTime = nowTime
                elif (events.button == 3):
                    self.rightButton = False
                    if nowTime - self.lastRightClickTime < self.doubleClickTime:
                        self.aziCorr = 0.0
                    self.lastRightClickTime = nowTime
            elif (events.type == pygame.MOUSEMOTION):
                if (self.leftButton):
                    currentMousePos = pygame.mouse.get_pos()
                    relX = currentMousePos[0] - self.mouseClickPos[0]
                    relY = currentMousePos[1] - self.mouseClickPos[1]
                    self.centerX += relX
                    self.centerY += relY
                    self.mouseClickPos = (currentMousePos[0], currentMousePos[1])
                elif (self.rightButton):
                    currentMousePos = pygame.mouse.get_pos()
                    currentAzi = math.atan2((currentMousePos[0] - self.centerX),(self.centerY - currentMousePos[1]))
                    deltaAzi = (currentAzi - self.rightClickAzi) * (180.0 / math.pi)
                    self.aziCorr -= deltaAzi
                    if self.aziCorr < 0:
                        self.aziCorr += 360.0
                    elif self.aziCorr > 360:
                        self.aziCorr -= 360.0
                    self.rightClickAzi = currentAzi
        
        return funcStopper
        
    #query command to get the device information parameters and display them to the terminal window
    def get_device_info(self):
        #self.COM.reset_input_buffer()
        commandBytes = '\xA5\x50'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        time.sleep(0.01)
        inputBuffer = self.COM.read(self.COM.inWaiting())
        inputDataAscii = binascii.hexlify(inputBuffer).decode('ascii')
        inputData = binascii.hexlify(inputBuffer)
        inputBytes = []
        for i in range(0, len(inputData), 2):
            inputBytes.append(inputData[i:i+2])

        read_byte8 = binascii.unhexlify(inputBytes[7])
        read_byte9 = binascii.unhexlify(inputBytes[8])
        read_byte10 = binascii.unhexlify(inputBytes[9])
        read_byte11 = binascii.unhexlify(inputBytes[10])

        model = int.from_bytes(read_byte8, byteorder='little')
        firmware_minor = int.from_bytes(read_byte9, byteorder='little')
        firmware_major = int.from_bytes(read_byte10, byteorder='little')
        hardware = int.from_bytes(read_byte11, byteorder='little')
        serialNum = inputDataAscii[22:]
        
        print("Get Device Info Response: ")
        print("   Model: " + str(model))
        print("   Firmware: " + str(firmware_major) + "." + str(firmware_minor))
        print("   Hardware: " + str(hardware))
        print("   Serial: " + serialNum)
        print("")

    #query command to get the device health parameters and display them to the terminal window
    def get_device_health(self):
        #self.COM.reset_input_buffer()
        commandBytes = '\xA5\x52'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        time.sleep(0.01)
        inputBuffer = self.COM.read(self.COM.inWaiting())
        #inputDataAscii = binascii.hexlify(inputBuffer).decode('ascii')     #DEBUGGING PURPOSES
        inputData = binascii.hexlify(inputBuffer)
        inputBytes = []
        for i in range(0, len(inputData), 2):
            inputBytes.append(inputData[i:i+2])

        read_byte8 = binascii.unhexlify(inputBytes[7])
        read_byte9 = binascii.unhexlify(inputBytes[8])
        read_byte10 = binascii.unhexlify(inputBytes[9])

        status = int.from_bytes(read_byte8, byteorder='little')
        error_code = int.from_bytes((read_byte9 + read_byte10), byteorder='little')
        
        print("Get Health Status Response: ")
        print("   Status: " + str(status))
        print("   Error Code: " + str(error_code))
        print("")

    #query command to get the device's default saaple rates and display them to the terminal window 
    def get_sample_rate(self):
        #self.COM.reset_input_buffer()
        commandBytes = '\xA5\x59'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        time.sleep(0.01)
        inputBuffer = self.COM.read(self.COM.inWaiting())
        #inputDataAscii = binascii.hexlify(inputBuffer).decode('ascii')     #DEBUGGING PURPOSES
        inputData = binascii.hexlify(inputBuffer)
        inputBytes = []
        for i in range(0, len(inputData), 2):
            inputBytes.append(inputData[i:i+2])

        read_byte8 = binascii.unhexlify(inputBytes[7])
        read_byte9 = binascii.unhexlify(inputBytes[8])
        read_byte10 = binascii.unhexlify(inputBytes[9])
        read_byte11 = binascii.unhexlify(inputBytes[10])

        Tstandard = int.from_bytes((read_byte8 + read_byte9), byteorder='little')
        Texpress = int.from_bytes((read_byte10 + read_byte11), byteorder='little')
        PtsPerSecStandard = 1.0 / (float(Tstandard) / 1000000.0)
        PtsPerSecExpress = 1.0 / (float(Texpress) / 1000000.0)

        self.pulseRateStandard = Tstandard
        self.pulseRateExpress = Texpress
        
        print("Get Sample Rate Response: ")
        print("   Standard Scan Sample Rate: " + str(Tstandard) + " us (" + str(int(PtsPerSecStandard)) + " pts/sec)")
        print("    Express Scan Sample Rate: " + str(Texpress) + " us (" + str(int(PtsPerSecExpress)) + " pts/sec)")
        print("")

    def start_standard_scan2(self):
        self.startTime = time.time()
        self.lastLeftClickTime = self.startTime
        self.lastRightClickTime = self.startTime
        self.lastMidClickTime = self.startTime

        stopper = False
        while not stopper:
            frame = self.data_receiver.get()
            if frame is None:
                time.sleep(0.001)
                continue
            for point in frame.points:
                self.totalNumPts += 1
                self.new_points.append(point)
            self.zeroPts = frame.zero_points
            self.rotationRate = frame.speed / 60
            self.scanCount += 1
            self.drawPoints()
            stopper = self.getControlInputs(stopper)
        pygame.quit()

    def get_one_frame_data_and_draw(self):
        if self.is_connected == False:
            self.drawPoints()
            return
        frame = self.data_receiver.get()
        if frame is None:
            return
        for point in frame.points:
            self.totalNumPts += 1
            self.new_points.append(point)
        self.zeroPts = frame.zero_points
        self.rotationRate = frame.speed / 60
        self.scanCount += 1
        self.drawPoints()

    def calcAndAddPoints(self,dist1,dist2,delta1,delta2,previousAngle,refAngle,txtOutput):
        angleDiff = float(refAngle - previousAngle)
        if (angleDiff) < 0:
            angleDiff += (math.pi * 2.0)
        
        deg90 = math.pi / 2.0
        
        pointCount = -1
        for i in range(0, 16):
            pointCount += 1
            if dist1[i] > 0:
                ptAngle1 = previousAngle + ((angleDiff / 32.0) * float(pointCount)) - delta1[i]
                X = round(math.sin(ptAngle1 + deg90) * dist1[i],3)
                Y = round(math.cos(ptAngle1 + deg90) * dist1[i],3)
                Xv = math.cos(ptAngle1 - (self.aziCorr * math.pi / 180.0)) * dist1[i]
                Yv = math.sin(ptAngle1 - (self.aziCorr * math.pi / 180.0)) * dist1[i]
                self.new_points.append([Xv,Yv])
                self.totalNumPts += 1
                txtOutput.write(str(self.scanCount) + "," + str(X) + "," + str(Y) + ",0\n")
            else:
                self.zeroPts += 1
            pointCount += 1
            if dist2[i] > 0:
                ptAngle2 = previousAngle + ((angleDiff / 32.0) * float(pointCount)) - delta2[i]
                X = round(math.sin(ptAngle2 + deg90) * dist2[i],3)
                Y = round(math.cos(ptAngle2 + deg90) * dist2[i],3)
                Xv = math.cos(ptAngle2 - (self.aziCorr * math.pi / 180.0)) * dist2[i]
                Yv = math.sin(ptAngle2 - (self.aziCorr * math.pi / 180.0)) * dist2[i]
                self.new_points.append([Xv,Yv])
                self.totalNumPts += 1
                txtOutput.write(str(self.scanCount) + "," + str(X) + "," + str(Y) + ",0\n")
            else:
                self.zeroPts += 1

    def readCabin1or3(self, data):
        distP2 = (data >> 2)
        deltaP1 = ((data & 1) << 4)
        deltaSign = -1*((data >> 1) & 1)
        if deltaSign == 0:
            deltaSign = 1
        return distP2, deltaP1, deltaSign
    
    def readCabin2or4(self, data):
        distP1 = (data << 6)   
        return distP1
    
    def readCabin5(self, data):
        delta2P2 = (data >> 4)
        delta1P2 = (data & 15)
        return delta1P2, delta2P2
    
    def calcDist(self, distP1, distP2):
        return (float(distP1 + distP2) / 1000.0)    #distance in meters
        
    def calcDelta(self, deltaP1, deltaP2, deltaSign):
        value = float(deltaSign) * (float(deltaP1 + deltaP2) / 8.0) * (math.pi/180.0)
        return (value)  #delta angle in radians
 
    # start motor to maximum rotation rate command (~20 Hz)
    def start_motor_max(self):
        commandBytes = '\xA5\xF0\x02\xFF\x03'
        #commandBytes = '\xA5\xF0\x02\x02\x00' #(super slow)
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Started (max speed)")
        print("")
        time.sleep(6.0)

    # start motor to 75% rotation rate command (~15 Hz)
    def start_motor_34(self):
        commandBytes = '\xA5\xF0\x02\x18\x03'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Started (3/4 speed)")
        print("")
        time.sleep(6.0)
    
    # start motor to half rotation rate command (~10 Hz)
    def start_motor_half(self):
        commandBytes = '\xA5\xF0\x02\x3F\x02'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Started (1/2 speed)")
        print("")
        time.sleep(6.0)

    # start motor to 25% rotation rate command (~5 Hz)
    def start_motor_14(self):
        commandBytes = '\xA5\xF0\x02\x4A\x01'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Started (1/4 speed)")
        print("")
        time.sleep(6.0)
    
    # start motor to minimum (useable) rotation rate command (~1.5 Hz)
    def start_motor_slow(self):
        commandBytes = '\xA5\xF0\x02\x80\x00'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Started (slow speed)")
        print("")
        time.sleep(6.0)

    # stop motor command (set to 0% rotation rate, 0 Hz)
    def stop_motor(self):
        commandBytes = '\xA5\xF0\x02\x00\x00'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Motor Stopped")
        print("")

    # stop scan command, stops the transmission of data
    def stop_scan(self):
        commandBytes = '\xA5\x25'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("Scan Stopped")
        print("")

    # reset command, should be used for proper communication/functionality checks, currently I'm not doing this
    def reset(self):
        commandBytes = '\xA5\x40'
        commandBytes = self.add_checksum(commandBytes)
        self.COM.write(commandBytes)
        print("System Reset")
        print("")


if __name__ == "__main__":
    try:
        sensor = RPyLIDAR(700, True, True)
        stopper = False
        while not stopper:
            sensor.get_one_frame_data_and_draw()
            stopper = sensor.getControlInputs(stopper)
        pygame.quit()
    except Exception as e:
        import traceback
        print(" *** AN UNKNOWN ERROR OCCURRED *** ")
        print(traceback.format_exc())
        sensor.disconnect()
        
        