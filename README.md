# TOFLidar

## Tools
- Stm32CubeIDE (1.12.1)
- STM32CubeMX (6.8.1)

## Debug
debug_print is disabled by default, uart3 is used to transfer data.
- firmware/FML/debug.h
  - ENABLE_DEBUG_PRINT

Control print info:
- main: firmware/Core/Src/main.c
```
#define DUMP_ENCODER 0
#define DUMP_ENCODER_PIN_COUNTERS 0
#define DUMP_TDC 1
#define DUMP_LIST 0
```

## Ros
- ros安装及代码编译参考
  - https://gitlab.com/lizhijie86su/pointcloud_viz_ros
  

- ros/src/cloudpoint_viz/config/viz_config.json
```
{
    "serial_port": "COM7",           # 根据情况调整
    "baud_rate": 230400,             # 固定
    "distance_magic_number": 41000   # 根据不同设备调整
}
```