
import sys
import os
import json
import socket
import threading
import serial
import math

import rclpy 
from rclpy.node import Node
import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

import numpy as np
import open3d as o3d

class LidarPoint(object):
    def __init__(self, distance, intencity, degree, distance_magic_number):
        self.distance = int(((distance * 1000) >> 8) * 0.3)    # mm
        if self.distance < distance_magic_number:
            self.distance = 0
        else:
            self.distance = (self.distance - distance_magic_number) >> 2
        self.intencity = intencity
        self.degree = degree

    def getXYZ(self):
        rad = math.radians(self.degree * 1.0 / 100)
        return (self.distance * np.cos(rad), self.distance * np.sin(rad), 0)

class FrameData(object):
    def __init__(self, index):
        self.index = index
        self.points = list()

class DataReceiver(object):
    FRAME_POOL_SIZE = 10
    PKG_HEAD = 0xA5
    PKG_TAIL = 0x5A
    POINTS_PER_PKG = 32
    PKG_HEAD_LEN = 3
    PKG_TAIL_LEN = 4
    PKG_SIZE = PKG_HEAD_LEN + 6 * POINTS_PER_PKG + PKG_TAIL_LEN

    def __init__(self, serial_port, baudrate, distance_magic_number):
        self.serial_port = serial_port
        self.baudrate = baudrate
        self.distance_magic_number = distance_magic_number
        self.ready_data_pool = list()

        self.serial = serial.Serial(serial_port, baudrate, timeout=0.1)
        self.th = threading.Thread(target=self.worker)
        self.th.daemon = True
        self.th.start()

    def get_one_packet(self):
        while True:
            try:
                data = self.serial.read(self.PKG_SIZE)
                if len(data) == self.PKG_SIZE and data[0] == self.PKG_HEAD and data[1] == self.PKG_HEAD and data[-1] == self.PKG_TAIL and data[-2] == self.PKG_TAIL:
                    return data
                else:
                    print("received invalid data. length = %d" % len(data))
            except Exception as e:
                print("Exception recv:", str(e))
                continue

    def start_get_packet(self):
        last_data = None
        while True:
            data = self.serial.read()
            if len(data) == 0:
                continue
            if data[0] == self.PKG_TAIL and last_data == self.PKG_TAIL:
                break
            last_data = data[0]

    def worker(self):
        frame_counter = 0
        print("start udp receving worker.", flush=True)
        self.start_get_packet()
        print("start...")
        while True:
            frame = FrameData(frame_counter)
            last_degree = 0.0
            while True:
                packet = self.get_one_packet()
                if packet[0] != self.PKG_HEAD or packet[-1] != self.PKG_TAIL:
                    print("invalid packet, drop it.")
                    continue
                for i in range(self.POINTS_PER_PKG):
                    distance = (int(packet[self.PKG_HEAD_LEN + i * 6 + 1]) << 8) | packet[self.PKG_HEAD_LEN + i * 6 + 0]
                    intencity = (int(packet[self.PKG_HEAD_LEN + i * 6 + 3]) << 8) | packet[self.PKG_HEAD_LEN + i * 6 + 2]
                    degree = (int(packet[self.PKG_HEAD_LEN + i * 6 + 5]) << 8) | packet[self.PKG_HEAD_LEN + i * 6 + 4]
                    if degree < last_degree:
                        print("frame %d is ready. points = %d" % (frame_counter, len(frame.points)), flush=True)
                        self.ready_data_pool.append(frame)
                        frame = FrameData(frame_counter)
                    else:
                        if distance != 0xFFFF and intencity != 0:
                            point = LidarPoint(distance, intencity, degree, self.distance_magic_number)
                            if point.distance > 0:
                                print("new point: D: %d, I: 0x%x, degree: %d" % (point.distance, intencity, degree), flush=True)
                                frame.points.append(point.getXYZ())
                            else:
                                print("invalid point.")
                    last_degree = degree

                frame_counter += 1
                print("get one frame: %d" % frame_counter, flush=True)
                if len(self.ready_data_pool) > self.FRAME_POOL_SIZE:
                    print("pop one frame since frame pool is full. size = %s" % (len(self.ready_data_pool)))
                    self.ready_data_pool.pop(0)

    def get(self):
        return self.ready_data_pool.pop(0) if len(self.ready_data_pool) > 0 else None


class CloudPointVizPublisher(Node):
    def __init__(self):
        super().__init__('cloudpoint_viz_publisher_node')
        self.config_file = sys.argv[1]
        self.resource_dir = sys.argv[2]
        config = json.load(open(self.config_file))
        serial_port = config["serial_port"]
        baud_rate = config["baud_rate"]
        distance_magic_number = config["distance_magic_number"]

        self.data_receiver = DataReceiver(serial_port, baud_rate, distance_magic_number)

        # I create a publisher that publishes sensor_msgs.PointCloud2 to the 
        # topic 'pcd'. The value '10' refers to the history_depth, which I 
        # believe is related to the ROS1 concept of queue size. 
        # Read more here: 
        # http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers
        self.viz_publisher = self.create_publisher(sensor_msgs.PointCloud2, 'pcd', 10)
        timer_period = 1/30.0
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        # Here I use the point_cloud() function to convert the numpy array 
        # into a sensor_msgs.PointCloud2 object. The second argument is the 
        # name of the frame the point cloud will be represented in. The default
        # (fixed) frame in RViz is called 'map'
        frame_data = self.data_receiver.get()
        if frame_data is not None:
            if len(frame_data.points) == 0:
                return
            #print(frame_data.points, flush=True)
            points = np.array([frame_data.points])
            #print("points shape: ", points.shape, flush=True)
            pcd = self.to_point_cloud(points, 'map')
            self.viz_publisher.publish(pcd)

    # def calc_data_cloud(self, raw_data):
    #     P = raw_data * 2.0 * 3.1416 / 256.0
    #     H = self.coeffs1 * np.square(P)+ self.coeffs2 * P + self.coeffs3
    #     H[(H > 800.0) | (H < 400.0)] = np.nan
    #     X = self.X_param * H
    #     Y = self.Y_param * H
    #     return np.stack((X, Y, H), axis=2)

    def to_point_cloud(self, points, parent_frame):
        """ Creates a point cloud message.
        Args:
            points: Frame-> [Row->[Point->[x,y,z],Point->[x,y,z],...]].
            parent_frame: frame in which the point cloud is defined
        Returns:
            sensor_msgs/PointCloud2 message

        Code source:
            https://gist.github.com/pgorczak/5c717baa44479fa064eb8d33ea4587e0

        References:
            http://docs.ros.org/melodic/api/sensor_msgs/html/msg/PointCloud2.html
            http://docs.ros.org/melodic/api/sensor_msgs/html/msg/PointField.html
            http://docs.ros.org/melodic/api/std_msgs/html/msg/Header.html

        """
        # In a PointCloud2 message, the point cloud is stored as an byte 
        # array. In order to unpack it, we also include some parameters 
        # which desribes the size of each individual point.
        ros_dtype = sensor_msgs.PointField.FLOAT32
        dtype = np.float32
        itemsize = np.dtype(dtype).itemsize # A 32-bit float takes 4 bytes.

        data = points.astype(dtype).tobytes() 

        # The fields specify what the bytes represents. The first 4 bytes 
        # represents the x-coordinate, the next 4 the y-coordinate, etc.
        fields = [sensor_msgs.PointField(
            name=n, offset=i*itemsize, datatype=ros_dtype, count=1)
            for i, n in enumerate('xyz')]

        # The PointCloud2 message also has a header which specifies which 
        # coordinate frame it is represented in. 
        header = std_msgs.Header(frame_id=parent_frame)

        return sensor_msgs.PointCloud2(
            header=header,
            height=points.shape[0],
            width=points.shape[1],
            is_dense=False,
            is_bigendian=False,
            fields=fields,
            point_step=(itemsize * points.shape[2]), # Every point consists of three float32s.
            row_step=(itemsize * points.shape[2] * points.shape[1]),
            data=data
        )

def main(args=None):
    rclpy.init(args=args)
    viz_publisher = CloudPointVizPublisher()
    rclpy.spin(viz_publisher)
    
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    viz_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
