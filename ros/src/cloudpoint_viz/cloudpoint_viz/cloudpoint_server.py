import socket
import time

UDP_IP = "127.0.0.1"
UDP_PORT = 5000
FRAME_WIDTH = 1280
FRAME_HEIGHT = 1024
ELEMENT_SIZE = 1
HEADER_SIZE = 8
FRAME_SIZE = FRAME_WIDTH * FRAME_HEIGHT * ELEMENT_SIZE

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 65536) 
udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65536)

def send_frame(frame_data, line_index):
    # Add header to the line
    data = bytearray()
    data.extend(map(ord, 'AA'))  # First two bytes: 'A' and 'B'
    data.extend(map(ord, 'BB'))
    data.extend((line_index >> 8).to_bytes(1, 'little'))  # Next two bytes: line index
    data.extend((line_index & 0xFF).to_bytes(1, 'little'))
    data.extend(map(ord, 'DD'))  # Last two bytes: 'D'
    data.extend(frame_data[line_index*FRAME_WIDTH:(line_index+1)*FRAME_WIDTH])
    # print(line_index, len(data))
    # Send the line with the header
    udp_socket.sendto(data, (UDP_IP, UDP_PORT))
    

def main():
    frame_file = '..\\resource\\viz\\pic_8bit.bin'  # Replace with your frame data file
    with open(frame_file, 'rb') as f:
        frame_data = f.read(FRAME_SIZE)
        line_index = 0
        while frame_data:
            send_frame(frame_data, line_index % FRAME_HEIGHT)
            line_index += 1
            # Delay to achieve 10 frames per second
            if line_index % FRAME_HEIGHT == 0:
                time.sleep(0.1)
                print("send frame: %d" % (line_index / FRAME_HEIGHT))

    udp_socket.close()
if __name__ == "__main__":
    main()
