from setuptools import setup

import os
from glob import glob

package_name = 'cloudpoint_viz'

setup(
    name=package_name,
    version='0.0.1',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # During installation, we need to copy the launch files
        (os.path.join('share', package_name, "launch"), glob('launch/cloudpoint_viz.launch.py')),
        # Same with the RViz configuration file.
        (os.path.join('share', package_name, "config"), glob('config/*')),
        # And the ply files.
        (os.path.join('share', package_name, "resource", "viz"), glob('resource/viz/*.bin')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ZhijieLi',
    maintainer_email='',
    description='visualize a point cloud using RViz',
    license='MIT License',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'cloudpoint_viz_subscriber_node = cloudpoint_viz.cloudpoint_viz:main'
        ],
    },
)