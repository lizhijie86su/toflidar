#ifndef __ENDOCER_H__
#define __ENDOCER_H__

#define DEBUG_ENCODER 1

#define ENCODER_PIN_COUNT 44

struct encoder_debug_info
{
    uint32_t state;  // 0 waiting, 1 running (stable)
    uint32_t total_cycles;
    uint32_t pin_count;
    uint16_t min_counter_per_pin;
    uint16_t max_counter_per_pin;
    uint16_t counters_per_pin[ENCODER_PIN_COUNT*2];
    uint16_t degrees[ENCODER_PIN_COUNT*2];

    int32_t pid_duty_delta;
    int32_t pid_duty;
    float pid_bias;
    float speed_hz;
};

#define MOTOR_SPEED 30.0

void init_encoder_mgr();

uint16_t get_current_degree();

struct encoder_debug_info get_encoder_debug_info();

float get_current_speed_hz();
#endif
