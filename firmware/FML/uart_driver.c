#include <string.h>
#include "usart.h"
#include "uart_driver.h"
#include "protocol.h"

#define VALID_UART_COUNT (USE_UART_1 + USE_UART_2 + USE_UART_3)

static uint8_t g_uart_valid[UART_MAX] = { USE_UART_1, USE_UART_2, USE_UART_3 };

static UART_HandleTypeDef* g_uart[UART_MAX] = { NULL, NULL, &huart3};
/////////////////////////////////////////////////////////////////////////////////////

#define UART_RX_BUFFER_SIZE 512

struct uart_driver_instance
{
  UART_HandleTypeDef *huart;
  on_uart_rx_f on_rx;
  on_uart_tx_f on_tx;

  uint8_t  tmp_recv_data;
  uint8_t  pkg_head_received;
  uint8_t  cur_buffer;
  uint16_t cur_offset;
  uint8_t  rx_bufer[2][UART_RX_BUFFER_SIZE];
};

#define UART_BUFFER_LEN (sizeof(struct uart_driver_instance) * VALID_UART_COUNT)

static uint8_t g_uart_buffer[UART_BUFFER_LEN];

static struct uart_driver_instance* g_uart_handler[UART_MAX] = { NULL, NULL, NULL };

uint16_t get_debug_uart_index()
{
  return DEBUG_UART;
}

uint16_t get_data_uart_index()
{
  return DATA_UART;
}

static void on_uart_rx(UART_HandleTypeDef *huart)
{
  for (uint16_t i = 0; i < UART_MAX; i ++) {
    if (!g_uart_valid[i]) {
        continue;
    }
    if (huart != g_uart_handler[i]->huart) {
        continue;
    }
    if (!g_uart_handler[i]->on_rx) {
        continue;
    }
    uint8_t data = huart->pRxBuffPtr[0];
    uint16_t cur_offset = g_uart_handler[i]->cur_offset;
    uint8_t cur_buffer = g_uart_handler[i]->cur_buffer;
    /*
     * Continuous PROTOCOL_PKG_HEAD will treated as normal data
     * */
    if (PROTOCOL_PKG_HEAD == data && 0 == g_uart_handler[i]->pkg_head_received) {
        g_uart_handler[i]->pkg_head_received = 1;
    } else if (g_uart_handler[i]->pkg_head_received) {
        if (IS_CMD(data)) {
          /* new package begin */
          g_uart_handler[i]->rx_bufer[cur_buffer][0] = PROTOCOL_PKG_HEAD;
          g_uart_handler[i]->rx_bufer[cur_buffer][1] = data;
          g_uart_handler[i]->cur_offset = 2;
        } else {
          /* treat PKG_HEAD as normal data*/
          g_uart_handler[i]->rx_bufer[cur_buffer][cur_offset] = PROTOCOL_PKG_HEAD;
          g_uart_handler[i]->rx_bufer[cur_buffer][cur_offset + 1] = data;
          g_uart_handler[i]->cur_offset += 2;
        }
        g_uart_handler[i]->pkg_head_received = 0;
    } else if (cur_offset > 2 && data == PROTOCOL_PKG_TAIL) {
        g_uart_handler[i]->on_rx(i, &g_uart_handler[i]->rx_bufer[cur_buffer][1], cur_offset - 1);
        g_uart_handler[i]->cur_buffer = (cur_buffer + 1) % 2;
        g_uart_handler[i]->cur_offset = 0;
    } else if(cur_offset > 2 && data != PROTOCOL_PKG_TAIL) {
        g_uart_handler[i]->rx_bufer[cur_buffer][cur_offset] = data;
        g_uart_handler[i]->cur_offset += 1;
    } else {
        g_uart_handler[i]->cur_offset = 0;
    }
    /*overwrite last byte if overflow until PKG_TAIL received*/
    if (g_uart_handler[i]->cur_offset == UART_RX_BUFFER_SIZE) {
        g_uart_handler[i]->cur_offset = UART_RX_BUFFER_SIZE - 1;
    }
    HAL_UART_Receive_DMA(huart, &g_uart_handler[i]->tmp_recv_data, 1);
  }
}

static void on_uart_tx(UART_HandleTypeDef *huart)
{
  for (uint16_t i = 0; i < UART_MAX; i ++) {
    if (!g_uart_valid[i]) {
        continue;
    }
    if (huart != g_uart_handler[i]->huart) {
        continue;
    }
    if (g_uart_handler[i]->on_tx) {
        g_uart_handler[i]->on_tx(i, huart->pTxBuffPtr, huart->TxXferSize);
        g_uart_handler[i]->on_tx = NULL;
        return;
    }
  }
}

HAL_StatusTypeDef init_uart_driver()
{
  HAL_StatusTypeDef status = HAL_OK;
  uint16_t cur_valid_uart_index = 0;
  memset(g_uart_buffer, '\0', UART_BUFFER_LEN);
  for (uint16_t i = 0; i < UART_MAX; i ++) {
    if (g_uart_valid[i]) {
      uint32_t offset = cur_valid_uart_index * sizeof(struct uart_driver_instance);
      g_uart_handler[i] = (struct uart_driver_instance*)&g_uart_buffer[offset];
      g_uart_handler[i]->huart = g_uart[i];
      g_uart_handler[i]->on_rx = NULL;
      g_uart_handler[i]->cur_buffer = 0;
      g_uart_handler[i]->cur_offset = 0;
      g_uart_handler[i]->pkg_head_received = 0;
      g_uart_handler[i]->tmp_recv_data = 0;
      status = HAL_UART_RegisterCallback(g_uart[i], HAL_UART_RX_COMPLETE_CB_ID, on_uart_rx);
      if (status != HAL_OK) {
          return status;
      }
      status = HAL_UART_RegisterCallback(g_uart[i], HAL_UART_TX_COMPLETE_CB_ID, on_uart_tx);
      if (status != HAL_OK) {
          return status;
      }
      /*start DMA receive*/
      status = HAL_UART_Receive_DMA(g_uart[i], g_uart_handler[i]->rx_bufer[0], 1);
      if (status != HAL_OK) {
          return status;
      }
      cur_valid_uart_index += 1;
    }
  }
  return HAL_OK;
}

HAL_StatusTypeDef uart_register_rx_cb(uint16_t uart_index, on_uart_rx_f cb)
{
  if (!g_uart_valid[uart_index]) {
      return HAL_ERROR;
  }
  g_uart_handler[uart_index]->on_rx = cb;
  return HAL_OK;
}

HAL_StatusTypeDef uart_send_async(uint16_t uart_index, uint8_t *buffer, uint16_t length, on_uart_tx_f on_tx)
{
  if (!g_uart_valid[uart_index]) {
      return HAL_ERROR;
  }
  g_uart_handler[uart_index]->on_tx = on_tx;
  return HAL_UART_Transmit_DMA(g_uart_handler[uart_index]->huart, buffer, length);
}

HAL_StatusTypeDef uart_send_sync(uint16_t uart_index, uint8_t *buffer, uint16_t length)
{
  if (!g_uart_valid[uart_index]) {
      return HAL_ERROR;
  }
  return HAL_UART_Transmit(g_uart_handler[uart_index]->huart, buffer, length, 10);
}
