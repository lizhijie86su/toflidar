#ifndef __DEBUG_H__
#define __DEBUG_H__

#define ENABLE_DEBUG_PRINT 0

#define UART_INDEX_DEBUG UART_3
/*
 * It is not thread safe, only use in main loop.
 */
void debug_printf(const char *fmt, ...);

void debug_uart_recv_callback(uint16_t uart_index, uint8_t *buf, uint16_t len);

#endif
