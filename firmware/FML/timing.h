#ifndef __TIMING__
#define __TIMING__

void start_timing();
uint32_t stop_and_get_timing_us();

#endif
