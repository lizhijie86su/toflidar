#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include "stdint.h"
//#include "flash_data.h"

#define PROTOCOL_PKG_HEAD       0xA5A5
#define PROTOCOL_PKG_TAIL       0x5A5A

#define CMD_NUMBER_MIN          0xC0

#define CMD_OBTAIN_DEVICE_INFO  0xC1
#define CMD_GET_PARAMETERS      0xC2
#define CMD_SET_PARAMETERS      0xC3
#define CMD_GET_STATE           0xC4

#define CMD_START_SCAN          0xD1
#define CMD_START_MEASURE       0xD2
#define CMD_STOP_SCAN           0xD3
#define CMD_STOP_MEASURE        0xD4

#define CMD_NUMBER_MAX          0xDF

#define CMD_MEASURED_DATA       0xEE

#define IS_CMD(CMD) ((CMD) > CMD_NUMBER_MIN && (CMD) < CMD_NUMBER_MAX)

struct HEAD
{
  uint16_t head;
  uint8_t cmd;
} __attribute__ ((packed));

struct TAIL
{
  uint16_t crc;
  uint16_t tail;
} __attribute__ ((packed));

struct device_info_pkg
{
  struct HEAD head;
  //struct device_info info;
  struct TAIL tail;
} __attribute__ ((packed));

struct device_params_pkg
{
  struct HEAD head;
  //struct device_params info;
  struct TAIL tail;
} __attribute__ ((packed));

struct device_state
{
  uint16_t state;
  uint32_t error_code;
  uint8_t resv[2];
} __attribute__ ((packed));

struct device_state_pkg
{
  struct HEAD head;
  struct device_state info;
  struct TAIL tail;
} __attribute__ ((packed));

struct reply_pkg
{
  struct HEAD head;
  uint32_t code;
  struct TAIL tail;
} __attribute__ ((packed));

#endif /* PROTOCOL_H_ */
