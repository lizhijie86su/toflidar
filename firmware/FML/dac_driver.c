#include "dac.h"

////////////////////////////////////////////////////////
//Hardware configure begin
extern DAC_HandleTypeDef hdac1;
//Hardware configure end
////////////////////////////////////////////////////////

const float c_dac_vref      = 2.5f;
const float c_comp_volt     = 0.5f; //PA4, PA5

const uint32_t c_dac_max    = 4095;

HAL_StatusTypeDef init_dac()
{
  HAL_StatusTypeDef status = HAL_OK;
  //PA4, PA5
  uint32_t comp_dac_v = c_dac_max * (c_comp_volt / c_dac_vref);
  status = HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, comp_dac_v);
  if (status != HAL_OK) {
      return status;
  }
  status = HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_2, DAC_ALIGN_12B_R, comp_dac_v);
  if (status != HAL_OK) {
      return status;
  }

  status = HAL_DAC_Start(&hdac1, DAC_CHANNEL_1);
  if (status != HAL_OK) {
      return status;
  }
  status = HAL_DAC_Start(&hdac1, DAC_CHANNEL_2);
  if (status != HAL_OK) {
      return status;
  }

  return HAL_OK;
}

void get_dac_values(uint32_t *laser_dac_v, uint32_t *commp_dac_v)
{
  *commp_dac_v = HAL_DAC_GetValue(&hdac1, DAC_CHANNEL_1);
  *laser_dac_v = HAL_DAC_GetValue(&hdac1, DAC_CHANNEL_2);
}
