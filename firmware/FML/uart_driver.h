#ifndef __UART_DRIVER_H__
#define __UART_DRIVER_H__
#include "usart.h"

enum
{
  UART_1 = 0,
  UART_2,
  UART_3,
  UART_MAX
};

#define DEBUG_UART UART_3

#define DATA_UART  UART_3

/*
 * These definitions are used to control which uart valid in system
 */
#define USE_UART_1 0
#define USE_UART_2 0
#define USE_UART_3 1

/*
 * @param1 uart_index
 * @param2 rx_buffer ptr
 * @param3 rx_buffer length
 *
 * Uart rx is based on the protocol of PKG_HEAD and PKG_TAIL.
 * Only content received between PKG_HEAD and PKG_TAIL will callback.
 * */
typedef uint32_t (*on_uart_rx_f)(uint16_t, uint8_t*, uint16_t);

/*
 * @param1 uart_index
 * @param2 tx_buffer ptr
 * @param3 tx_buffer length
 *
 *
 * */
typedef void (*on_uart_tx_f)(uint16_t, const uint8_t*, uint16_t);

uint16_t get_debug_uart_index();

uint16_t get_data_uart_index();

HAL_StatusTypeDef init_uart_driver();

HAL_StatusTypeDef uart_register_rx_cb(uint16_t uart_index, on_uart_rx_f cb);

HAL_StatusTypeDef uart_send_async(uint16_t uart_index, uint8_t *buffer, uint16_t length, on_uart_tx_f on_tx);

HAL_StatusTypeDef uart_send_sync(uint16_t uart_index, uint8_t *buffer, uint16_t length);

#endif
