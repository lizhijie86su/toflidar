#include "tim.h"

TIM_HandleTypeDef *g_timing_timer      = &htim7;        // static config

void start_timing()
{
    __HAL_TIM_SET_COUNTER(g_timing_timer, 0);
    HAL_TIM_Base_Start(g_timing_timer);
}

uint32_t stop_and_get_timing_us()
{
    HAL_TIM_Base_Stop(g_timing_timer);
    uint32_t counter = __HAL_TIM_GET_COUNTER(g_timing_timer);
    return counter / 10;
}
