#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "debug.h"
#include "uart_driver.h"

#define min(a, b) ((a) < (b) ? (a) : (b))

#define MAX_DEBUG_MSG_BUFFER_SIZE 128
static char buffer[MAX_DEBUG_MSG_BUFFER_SIZE];

void debug_printf(const char *fmt, ...)
{
#if (ENABLE_DEBUG_PRINT)
    memset(buffer, '\0', sizeof(buffer));
    va_list ap;
    va_start(ap, fmt);
    uint32_t len = vsnprintf(buffer, sizeof(buffer), (const char*)fmt, ap);
    va_end(ap);
    uart_send_sync(UART_INDEX_DEBUG, (uint8_t*)buffer, len);
#endif
}
