#ifndef TDC_DATA_MGR_H_
#define TDC_DATA_MGR_H_

#include "tdc_driver.h"

struct tdc_data_debug_info
{
  uint32_t max_read_tdc_time_us;
  uint32_t min_read_tdc_time_us;
  uint32_t total_read_tdc_time_us;
  uint32_t total_received_points;
  uint32_t total_dropped_points;
  uint32_t success_send_groups;
  uint32_t failed_send_groups;
  uint8_t  empty_list_length;
  uint8_t  full_list_length;
  uint8_t  sending_list_length;
  Lidar_PointTypeDef last_point;
  TDC_RESTypeDef last_resgiter;
};

void tdc_mgr_init();

void tdc_mgr_send_data_once();

struct tdc_data_debug_info get_tdc_data_debug_info();

#endif /* TDC_DATA_MGR_H_ */
