#include <stdint.h>
#include <assert.h>
#include <math.h>
#include "encoder.h"
#include "tim.h"
#include "debug.h"
#include "delay.h"

// Encoder: 43 normal pin + 1 long pin
//
#define DEBUG_ENCODER 0
#define DEBUG_PID 0

TIM_HandleTypeDef *g_encoder_motor_pwm_timer    = &htim3;        // static config
const int g_encoder_moter_timer_channel         = TIM_CHANNEL_3; // static config

TIM_HandleTypeDef *g_encoder_counter_timer      = &htim6;        // static config

const float c_motor_speed_stable_threshold        = 0.5f;

enum {
    ENCODER_STATE_WAITING = 0,
    ENCODER_STATE_RUNNING
};

struct pid_param
{
    float kp;
    float ki;
    float kd;

    float last_bias1;
    float last_bias2;

    float motor_target_speed_hz;
    uint32_t motor_pwm_init_duty;
};

struct encoder_mgr_t
{
    /*! current pause index inside one cycle */
    uint32_t pin_index;
    uint32_t last_timer_total_count;
    uint32_t current_timer_count;
    float speed_hz;

    /*! the time point (in milliseconds) for last zero point */
    uint32_t last_zero_point_time_ms;

    /*! timer counter for last pause */
    uint32_t last_pulse_tim_counter;

    /*! ENCODER_STATE_XXX */
    uint32_t encoder_state;

    struct pid_param pid;

    struct encoder_debug_info debug_info;
    struct encoder_debug_info debug_info_per_cycle;
};

struct encoder_mgr_t g_encoder_mgr;

static void init_debug_info(struct encoder_debug_info *info)
{
    info->state = ENCODER_STATE_WAITING;
    info->total_cycles = 0;
    info->speed_hz = .0f;
    info->pin_count = 0;
    info->min_counter_per_pin = 65535;
    info->max_counter_per_pin = 0;
    info->pid_bias = 0.0;
    info->pid_duty = 0;
    info->pid_duty_delta = 0;
    for (int i = 0; i < ENCODER_PIN_COUNT * 2; i ++)
    {
        info->counters_per_pin[i] = 0;
        info->degrees[i] = 0;
    }
}

void init_encoder_mgr()
{
    g_encoder_mgr.pin_index = 0;
    g_encoder_mgr.last_timer_total_count = 0;
    g_encoder_mgr.current_timer_count = 0;
    g_encoder_mgr.last_zero_point_time_ms = 0;
    g_encoder_mgr.last_pulse_tim_counter = 0;
    g_encoder_mgr.encoder_state = ENCODER_STATE_WAITING;
    g_encoder_mgr.speed_hz = 0.0;

    init_debug_info(&g_encoder_mgr.debug_info);

	g_encoder_mgr.pid.kp = 1.0f;
	g_encoder_mgr.pid.ki = 8.0f;
	g_encoder_mgr.pid.kd = 1.0f;
	g_encoder_mgr.pid.last_bias1 = .0f;
	g_encoder_mgr.pid.last_bias2 = .0f;
	g_encoder_mgr.pid.motor_target_speed_hz = MOTOR_SPEED;
	g_encoder_mgr.pid.motor_pwm_init_duty = 170;

    // Motor timer
    __HAL_TIM_SET_COMPARE(g_encoder_motor_pwm_timer, g_encoder_moter_timer_channel, g_encoder_mgr.pid.motor_pwm_init_duty);
    HAL_TIM_PWM_Start(g_encoder_motor_pwm_timer, g_encoder_moter_timer_channel);

    // Encoder counter timer
    HAL_TIM_Base_Start(g_encoder_counter_timer);
}

struct encoder_debug_info get_encoder_debug_info()
{
    return g_encoder_mgr.debug_info_per_cycle;
}

uint16_t get_current_degree()
{
    uint32_t count = g_encoder_mgr.current_timer_count + __HAL_TIM_GET_COUNTER(g_encoder_counter_timer);
    return (uint16_t)(((float)count / g_encoder_mgr.last_timer_total_count) * 36000);
}

float get_current_speed_hz()
{
    return g_encoder_mgr.speed_hz;
}
/*
 * Return 1 is motor speed is stable
 */
static uint8_t check_speed_stable(float current_speed_hz)
{
#define SPEED_BUFFERS 8
    static uint32_t pos = 0;
    static float speed_buf[SPEED_BUFFERS] = { .0f };

    speed_buf[(pos ++) % SPEED_BUFFERS] = current_speed_hz;
    if (pos < SPEED_BUFFERS)
        return 0;

    float total_speed = .0f;
    float average_speed = 0.0f;
    for (uint8_t i = 0; i < SPEED_BUFFERS; i++) {
        total_speed += speed_buf[i];
    }
    average_speed = total_speed / SPEED_BUFFERS;

    for (uint8_t i = 0; i < SPEED_BUFFERS; i++) {
        if (fabs(average_speed - speed_buf[i]) > c_motor_speed_stable_threshold) {
            return 0;
        }
    }
    return 1;
}

static void control_motor_pid(float current_speed_hz)
{
    float bias = g_encoder_mgr.pid.motor_target_speed_hz - current_speed_hz;
    int32_t duty_delta = g_encoder_mgr.pid.kp * (bias - g_encoder_mgr.pid.last_bias1)
                       + g_encoder_mgr.pid.ki * bias
                       + g_encoder_mgr.pid.kd * (bias - 2*g_encoder_mgr.pid.last_bias1 + g_encoder_mgr.pid.last_bias2);
    int32_t duty = g_encoder_mgr.pid.motor_pwm_init_duty + duty_delta;
#if (DEBUG_ENCODER)
    g_encoder_mgr.debug_info.pid_duty = duty;
    g_encoder_mgr.debug_info.pid_duty_delta = duty_delta;
    g_encoder_mgr.debug_info.pid_bias = bias;
#endif
    if (duty < 50) duty = 50;
    if (duty > 500) duty = 500;
    __HAL_TIM_SET_COMPARE(g_encoder_motor_pwm_timer, g_encoder_moter_timer_channel, duty);
    g_encoder_mgr.pid.last_bias2 = g_encoder_mgr.pid.last_bias1;
    g_encoder_mgr.pid.last_bias1 = bias;
#if (DEBUG_PID)
    debug_printf("PID pwm: %d\n", pwm);
#endif
}

void ENCODER_HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    HAL_TIM_Base_Stop(g_encoder_counter_timer);
    uint32_t counter = __HAL_TIM_GET_COUNTER(g_encoder_counter_timer);
    __HAL_TIM_SET_COUNTER(g_encoder_counter_timer, 0);
    HAL_TIM_Base_Start(g_encoder_counter_timer);

    // check zero point
    if (counter > 2 * g_encoder_mgr.last_pulse_tim_counter) {
        uint32_t current_time_ms = HAL_GetTick();
        float current_speed_hz = 1000.0f / (current_time_ms - g_encoder_mgr.last_zero_point_time_ms);
        g_encoder_mgr.speed_hz = current_speed_hz;
        if (g_encoder_mgr.encoder_state == ENCODER_STATE_WAITING) {
            if (check_speed_stable(current_speed_hz)) {
                g_encoder_mgr.encoder_state = ENCODER_STATE_RUNNING;
#if (DEBUG_ENCODER)
                g_encoder_mgr.debug_info.state = ENCODER_STATE_RUNNING;
#endif
                debug_printf("Encoder motor is stable\n");
            }
        } else {
            control_motor_pid(current_speed_hz);
        }
#if (DEBUG_ENCODER)
        g_encoder_mgr.debug_info.total_cycles += 1;
        g_encoder_mgr.debug_info.speed_hz = current_speed_hz;
        g_encoder_mgr.debug_info.pin_count = g_encoder_mgr.pin_index + 1;

        g_encoder_mgr.debug_info_per_cycle = g_encoder_mgr.debug_info;

        g_encoder_mgr.debug_info.max_counter_per_pin = 0;
        g_encoder_mgr.debug_info.min_counter_per_pin = 65535;
        for (int i = 0; i < ENCODER_PIN_COUNT * 2; i ++)
        {
            g_encoder_mgr.debug_info.counters_per_pin[i] = 0;
            g_encoder_mgr.debug_info.degrees[i] = 0;
        }
#endif
        g_encoder_mgr.last_timer_total_count = g_encoder_mgr.current_timer_count + counter;
        g_encoder_mgr.last_zero_point_time_ms = current_time_ms;
        g_encoder_mgr.pin_index = 0;
        g_encoder_mgr.current_timer_count = 0;
    } else {
        g_encoder_mgr.pin_index += 1;
        g_encoder_mgr.current_timer_count += counter;
    }

    g_encoder_mgr.last_pulse_tim_counter = counter;

#if (DEBUG_ENCODER)
    g_encoder_mgr.debug_info.counters_per_pin[g_encoder_mgr.pin_index % (ENCODER_PIN_COUNT * 2)] = counter;
    g_encoder_mgr.debug_info.degrees[g_encoder_mgr.pin_index % (ENCODER_PIN_COUNT * 2)] = get_current_degree();
    if (counter < g_encoder_mgr.debug_info.min_counter_per_pin){
        g_encoder_mgr.debug_info.min_counter_per_pin = counter;
    }
    if (counter > g_encoder_mgr.debug_info.max_counter_per_pin){
        g_encoder_mgr.debug_info.max_counter_per_pin = counter;
    }
#endif
}
