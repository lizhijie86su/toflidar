#include <stdint.h>

#include "main.h"
#include "gpio.h"
#include "bsp.h"

extern void TDC_INT_HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

extern void ENCODER_HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (ENCODER_EXTI_Pin == GPIO_Pin) {
        ENCODER_HAL_GPIO_EXTI_Callback(GPIO_Pin);
    } else if (TDC1_INTN_Pin == GPIO_Pin){
        TDC_INT_HAL_GPIO_EXTI_Callback(GPIO_Pin);
    }
}
