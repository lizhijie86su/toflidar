#include "tim.h"

/*
 * htim1 ch3: APD_PWM
 * 100k hz, period = 1600, duty = 20% (320)
 * */

#define APD_PWM_DUTY 320

HAL_StatusTypeDef init_apd_pwm()
{
  HAL_StatusTypeDef status = HAL_OK;

  __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, 320);

  status = HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
  if (status != HAL_OK) {
      return status;
  }

  return HAL_OK;
}
