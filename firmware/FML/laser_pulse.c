#include "tim.h"
#include "laser_pulse.h"

/*
 * htim16 ch1: PAUSE_START
 * */

HAL_StatusTypeDef init_laser_pulse()
{
  HAL_StatusTypeDef status = HAL_OK;

  __HAL_TIM_SET_AUTORELOAD(&htim16, LASER_PULSE_TIM_PRELOAD);
  __HAL_TIM_SET_COMPARE(&htim16, TIM_CHANNEL_1, 2);

  status = HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);
  if (status != HAL_OK) {
      return status;
  }

  return HAL_OK;
}
