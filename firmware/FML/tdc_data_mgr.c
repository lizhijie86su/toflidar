#include "list.h"
#include "protocol.h"
#include "tdc_data_mgr.h"
#include "uart_driver.h"
#include "meas_dist.h"
#include "encoder.h"
#include "tdc_driver.h"
#include "timing.h"
#include "laser_pulse.h"

#define TDC_SEND_DATA_USE_UART 1
#define TDC_SEND_DATA_USE_UDP 0
#define USE_DATA2 1

#define DEBUG_TDC_DATA 0

#define SEND_POINTS_PER_GROUP 32

#define GROUP_POOL_SIZE 16

#define DISTANCE_MAGIC_NUMBER 4100

#define BEGIN_INVALID_ANGLE 11000 // 0.01 Degree
#define END_INVALID_ANGLE 20000
// 333
#define POINTS_PER_CYCLE (LASER_PULSE_FREQ / MOTOR_SPEED)

struct packed_tdc_data
{
  struct HEAD head;
  Lidar_PointTypeDef points[SEND_POINTS_PER_GROUP];
  struct TAIL tail;
} __attribute__ ((packed));


struct packed_tdc_data2
{
  uint8_t head[2];
  uint8_t type;
  uint8_t points;
  uint16_t beg_angle;
  uint16_t end_angle;
  uint16_t speed;
  uint16_t data[SEND_POINTS_PER_GROUP];
  uint16_t crc;
  uint8_t tail[2];
}__attribute__ ((packed));

uint16_t CRC_Check(uint8_t *buf, uint16_t size)
{
  uint16_t check_sum = 0;
  uint16_t i = 0;
  if (size <= 0)
  {
    return check_sum;
  }
  for (i = 0; i < size; i ++)
  {
    check_sum += buf[i];
  }
  return check_sum;
}

uint8_t convert_data_data2(struct packed_tdc_data* data, struct packed_tdc_data2 *data2, float speed_hz)
{
  uint8_t valid_data_number = 0;
  data2->head[0] = 0xFA;
  data2->head[1] = 0xF5;
  data2->type = 0;
  data2->points = SEND_POINTS_PER_GROUP;

  data2->beg_angle = data->points[0].degree;
  data2->end_angle = data->points[SEND_POINTS_PER_GROUP - 1].degree;
  data2->speed = (uint16_t)(speed_hz * 60);
  for (int i = 0; i < SEND_POINTS_PER_GROUP; i ++) {
    if (data->points[i].degree > BEGIN_INVALID_ANGLE && data->points[i].degree < END_INVALID_ANGLE)
    {
        data2->data[i] = 0;
    }
    else
    {
        data2->data[i] = ((uint16_t)(((data->points[i].distance * 1000) >> 8) * 0.3)) >> 1;
        if (data2->data[i] > DISTANCE_MAGIC_NUMBER)
        {
            data2->data[i] -= DISTANCE_MAGIC_NUMBER;
        }
        else
        {
            data2->data[i] = 0;
        }
        valid_data_number += 1;
    }
  }
  data2->crc = CRC_Check((uint8_t*)data2, sizeof(struct packed_tdc_data2) - 4);
  data2->tail[0] = 0x31;
  data2->tail[1] = 0xF2;
  return valid_data_number;
}

struct tdc_data_group
{
  struct list_head node;
  uint16_t offset;
  struct packed_tdc_data data;
  struct packed_tdc_data2 data2;
};

struct tdc_mgr
{
  struct list_head empty_list;

  /* add to tail, send from head */
  struct list_head full_list;

  struct list_head sending_list;

  struct tdc_data_group tdc_data_pool[GROUP_POOL_SIZE];

#if (TDC_SEND_DATA_USE_UART)
  uint16_t send_data_uart;
#endif

#if (DEBUG_TDC_DATA)
  struct tdc_data_debug_info debug_info;
#endif
};

static struct tdc_mgr g_tdc_mgr;

/* Two types of interrupt may happen:
 * 1. TDC EXTI interrupt
 * 2. Uart transmit complete interrupt
 *
 * TODO: fix hardcode
 * */
static void inline __tdc_data_disable_irq()
{
  __disable_irq();
}

static void inline __tdc_data_enable_irq()
{
  __enable_irq();
}

#if (DEBUG_TDC_DATA)
static void init_debug_info(struct tdc_data_debug_info *info)
{
  info->total_read_tdc_time_us = 0;
  info->min_read_tdc_time_us = 100000;
  info->max_read_tdc_time_us = 0;
  info->total_received_points = 0;
  info->total_dropped_points = 0;
  info->success_send_groups = 0;
  info->failed_send_groups = 0;
  info->empty_list_length = GROUP_POOL_SIZE;
  info->full_list_length = 0;
  info->sending_list_length = 0;
  info->last_point.distance = 0;
  info->last_point.intensity = 0;
  info->last_resgiter.RES_0 = 0;
  info->last_resgiter.RES_1 = 0;
  info->last_resgiter.STAT_0 = 0;
  info->last_resgiter.STAT_1 = 0;
}
#endif
/*
 * recycle the buffer
 * sending_list -> empty_list
 * */
static void on_data_send_out_done(uint16_t uart_index, const uint8_t* buffer, uint16_t length)
{
#if USE_DATA2
  struct tdc_data_group *data_group = container_of(buffer, struct tdc_data_group, data2);
#else
  struct tdc_data_group *data_group = container_of(buffer, struct tdc_data_group, data);
#endif
  data_group->offset = 0;
  __tdc_data_disable_irq();
  list_del(&data_group->node);
  list_add_tail(&data_group->node, &g_tdc_mgr.empty_list);

#if (DEBUG_TDC_DATA)
  g_tdc_mgr.debug_info.sending_list_length -= 1;
  g_tdc_mgr.debug_info.empty_list_length += 1;
#endif
  __tdc_data_enable_irq();
}

void tdc_mgr_send_data_once()
{
  __tdc_data_disable_irq();
  if (list_empty(&g_tdc_mgr.full_list)) {
      __tdc_data_enable_irq();
      return;
  } else {
      struct tdc_data_group *send_data_group = (struct tdc_data_group*)g_tdc_mgr.full_list.next;
      list_del(&send_data_group->node);
      list_add(&send_data_group->node, &g_tdc_mgr.sending_list);
#if (DEBUG_TDC_DATA)
      g_tdc_mgr.debug_info.full_list_length -= 1;
      g_tdc_mgr.debug_info.sending_list_length += 1;
#endif
      __tdc_data_enable_irq();

      /* send data */
#if (TDC_SEND_DATA_USE_UART)
    #if USE_DATA2
      uint8_t valid_points = convert_data_data2(&send_data_group->data, &send_data_group->data2, get_current_speed_hz());
      int ret = 0;
      if (valid_points > 0)
      {
          ret = uart_send_async(g_tdc_mgr.send_data_uart, (uint8_t*)&send_data_group->data2,
                                sizeof(struct packed_tdc_data2), on_data_send_out_done);
      }
      else
      {
          ret = -1;
      }
    #else
      int ret = uart_send_async(g_tdc_mgr.send_data_uart, (uint8_t*)&send_data_group->data,
                            sizeof(struct packed_tdc_data), on_data_send_out_done);
    #endif
#else
      int ret = -1;
#endif

      if (0 != ret) {
          /* if send fail, move the data empty list, data is lost*/
    #if USE_DATA2
          on_data_send_out_done(0, (uint8_t*)&send_data_group->data2, sizeof(struct packed_tdc_data));
    #else
          on_data_send_out_done(0, (uint8_t*)&send_data_group->data, sizeof(struct packed_tdc_data));
    #endif
#if (DEBUG_TDC_DATA)
          if (ret != -1)
              g_tdc_mgr.debug_info.failed_send_groups += 1;
#endif

      } else {

#if (DEBUG_TDC_DATA)
          g_tdc_mgr.debug_info.success_send_groups += 1;
#endif
      }
  }
}

void TDC_INT_HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  __tdc_data_disable_irq();
  struct tdc_data_group *recv_group = (struct tdc_data_group*)g_tdc_mgr.empty_list.next;
  if (list_empty(&g_tdc_mgr.empty_list)) {
      __tdc_data_enable_irq();
#if (DEBUG_TDC_DATA)
  g_tdc_mgr.debug_info.total_dropped_points += 1;
#endif
      return;
  } else {
      __tdc_data_enable_irq();
  }

#if (DEBUG_TDC_DATA)
  start_timing();
  TDC_RESTypeDef tdc_res = TDC_Meas2NoWait(1);
#else
  TDC_RESTypeDef tdc_res = TDC_Meas2NoWait(0);
#endif

  Meas_DataTransfer(&tdc_res, &recv_group->data.points[recv_group->offset]);
  recv_group->data.points[recv_group->offset].degree = get_current_degree();

#if (DEBUG_TDC_DATA)
  uint32_t read_time = stop_and_get_timing_us();
  g_tdc_mgr.debug_info.total_received_points += 1;
  g_tdc_mgr.debug_info.total_read_tdc_time_us += read_time;
  if (read_time < g_tdc_mgr.debug_info.min_read_tdc_time_us)
      g_tdc_mgr.debug_info.min_read_tdc_time_us = read_time;
  if (read_time > g_tdc_mgr.debug_info.max_read_tdc_time_us)
      g_tdc_mgr.debug_info.max_read_tdc_time_us = read_time;
  g_tdc_mgr.debug_info.last_point = recv_group->data.points[recv_group->offset];
  g_tdc_mgr.debug_info.last_resgiter = tdc_res;
#endif

  recv_group->offset += 1;
  if (SEND_POINTS_PER_GROUP == recv_group->offset) {
      __tdc_data_disable_irq();
      list_del(&recv_group->node);
      list_add_tail(&recv_group->node, &g_tdc_mgr.full_list);
#if (DEBUG_TDC_DATA)
      g_tdc_mgr.debug_info.empty_list_length -= 1;
      g_tdc_mgr.debug_info.full_list_length += 1;
#endif
      __tdc_data_enable_irq();
  }
}

void tdc_mgr_init()
{
  INIT_LIST_HEAD(&g_tdc_mgr.empty_list);
  INIT_LIST_HEAD(&g_tdc_mgr.full_list);
  INIT_LIST_HEAD(&g_tdc_mgr.sending_list);
  for (uint8_t i = 0; i < GROUP_POOL_SIZE; i ++) {
      g_tdc_mgr.tdc_data_pool[i].offset = 0;
      g_tdc_mgr.tdc_data_pool[i].data.head.head = PROTOCOL_PKG_HEAD;
      g_tdc_mgr.tdc_data_pool[i].data.tail.tail = PROTOCOL_PKG_TAIL;
      list_add(&g_tdc_mgr.tdc_data_pool[i].node, &g_tdc_mgr.empty_list);
  }

#if (TDC_SEND_DATA_USE_UART)
  g_tdc_mgr.send_data_uart = get_data_uart_index();
#endif

#if (DEBUG_TDC_DATA)
  init_debug_info(&g_tdc_mgr.debug_info);
#endif
}

struct tdc_data_debug_info get_tdc_data_debug_info()
{
  struct tdc_data_debug_info debug_info;
#if (DEBUG_TDC_DATA)
  __tdc_data_disable_irq();
  debug_info = g_tdc_mgr.debug_info;
  __tdc_data_enable_irq();
#else
  debug_info.sending_list_length = 0; // prevent warning
#endif
  return debug_info;
}
