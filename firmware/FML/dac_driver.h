#ifndef __DAC_DRIVER_H__
#define __DAC_DRIVER_H__

HAL_StatusTypeDef init_dac();

void get_dac_values(uint32_t *laser_dac_v, uint32_t *commp_dac_v);

#endif
