

#ifndef __BOARD_H__
#define __BOARD_H__

#include "stm32g4xx_hal.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "bsp.h"

#include "delay.h"

const static uint16_t systemclock = 160;  //system  Clock

#define RX_BUFF_MAX  64

// SPAD
#define SPAD_RX_SIZE           RX_BUFF_MAX
extern u8 SPAD_Uart_Rx_Buffer[SPAD_RX_SIZE];
extern u8 SPAD_Uart_Rx_Data[SPAD_RX_SIZE];

extern __IO u8 SPAD_Uart_Rx_Flag;
extern __IO u8 SPAD_Uart_Rx_Len;

//APD PWM GAIN
#define  HV_TIM_PULSE               160
#define  HV_TIM_PERIOD              1500


// flash
#define FLASH_TYPEPROGRAM_WORD        ((uint32_t)0x05U) //hacking

#ifndef __nop
#define __nop __NOP
#endif
#endif  
