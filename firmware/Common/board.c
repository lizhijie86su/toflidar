#include "board.h"
#include "bsp.h"

u8 SPAD_Uart_Rx_Buffer[SPAD_RX_SIZE] = {0};
u8 SPAD_Uart_Rx_Data[SPAD_RX_SIZE] = {0};
uint8_t SPAD_RingBuffer[SPAD_RX_SIZE] = {0};

__IO u8 SPAD_Uart_Rx_Flag = 0;
__IO u8 SPAD_Uart_Rx_Len = 0;
