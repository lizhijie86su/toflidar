/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "usb.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "board.h"
#include "bsp.h"
#include "tdc_driver.h"
#include "meas_dist.h"
#include "tdc_data_mgr.h"
#include "dac_driver.h"
#include "encoder.h"
#include "uart_driver.h"
#include "debug.h"
#include "laser_pulse.h"
#include "apd_pwm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define DUMP_ENCODER 1
#define DUMP_ENCODER_PIN_COUNTERS 0
#define DUMP_ENCODER_DEGREES 1
#define DUMP_TDC 0
#define DUMP_LIST 0
void dump_debug_info()
{
  static uint32_t last_dump_time_ms = 0;
  static struct tdc_data_debug_info last_tdc_info;
#if (DUMP_ENCODER)
  static struct encoder_debug_info last_encoder_info;
#endif

  if (last_dump_time_ms == 0)
  {
    last_dump_time_ms = HAL_GetTick();
    return;
  }

  uint32_t cur_time_ms = HAL_GetTick();
  uint32_t duration_ms = cur_time_ms - last_dump_time_ms;
  if (duration_ms > 200)
  {
    debug_printf("********\n");
#if (DUMP_ENCODER)
    struct encoder_debug_info cur_encoder_info = get_encoder_debug_info();
    debug_printf("ENC[Stat:%d,Freq:%f/%f,Puls:%d,PinCounter(%d,%d)]PID[bias:%f,duty:%d,delta:%d]\n",
        cur_encoder_info.state,
        (float)(cur_encoder_info.total_cycles - last_encoder_info.total_cycles) * (1000.0f / duration_ms),
        cur_encoder_info.speed_hz,
        cur_encoder_info.pin_count,
        cur_encoder_info.min_counter_per_pin,
        cur_encoder_info.max_counter_per_pin,
        cur_encoder_info.pid_bias,
        cur_encoder_info.pid_duty,
        cur_encoder_info.pid_duty_delta);
    #if (DUMP_ENCODER_PIN_COUNTERS)
    for (int i = 0; i < ENCODER_PIN_COUNT * 2; i += 8)
    {
        debug_printf("%08d %08d %08d %08d %08d %08d %08d %08d \n",
          cur_encoder_info.counters_per_pin[i],
          cur_encoder_info.counters_per_pin[i + 1],
          cur_encoder_info.counters_per_pin[i + 2],
          cur_encoder_info.counters_per_pin[i + 3],
          cur_encoder_info.counters_per_pin[i + 4],
          cur_encoder_info.counters_per_pin[i + 5],
          cur_encoder_info.counters_per_pin[i + 6],
          cur_encoder_info.counters_per_pin[i + 7]);
    }
    #endif
	#if (DUMP_ENCODER_DEGREES)
	for (int i = 0; i < ENCODER_PIN_COUNT * 2; i += 8)
	{
		debug_printf("%08d %08d %08d %08d %08d %08d %08d %08d \n",
		  cur_encoder_info.degrees[i],
		  cur_encoder_info.degrees[i + 1],
		  cur_encoder_info.degrees[i + 2],
		  cur_encoder_info.degrees[i + 3],
		  cur_encoder_info.degrees[i + 4],
		  cur_encoder_info.degrees[i + 5],
		  cur_encoder_info.degrees[i + 6],
		  cur_encoder_info.degrees[i + 7]);
	}
	#endif

    last_encoder_info = cur_encoder_info;
#endif

    struct tdc_data_debug_info cur_tdc_info = get_tdc_data_debug_info();
#if (DUMP_TDC)
    uint32_t points = cur_tdc_info.total_received_points - last_tdc_info.total_received_points;
    debug_printf("TDC Rtime[min:%d,max:%d,avg:%d]us P/s:%d Dat[D:%x,I:%x]ns REG[%x,%x], STAT[%x,%x]\n",
        cur_tdc_info.min_read_tdc_time_us,
        cur_tdc_info.max_read_tdc_time_us,
        points == 0 ? 0 : (cur_tdc_info.total_read_tdc_time_us - last_tdc_info.total_read_tdc_time_us) / points,
        points * 1000 / duration_ms,
        //cur_tdc_info.total_dropped_points - last_tdc_info.total_dropped_points,
        cur_tdc_info.last_point.distance,
        cur_tdc_info.last_point.intensity,
        cur_tdc_info.last_resgiter.RES_0,
        cur_tdc_info.last_resgiter.RES_1,
        cur_tdc_info.last_resgiter.STAT_0,
        cur_tdc_info.last_resgiter.STAT_1);
#endif

#if (DUMP_LIST)
    debug_printf("Grp[S:%d,F:%d] List[E:%d,F:%d,S:%d]\n",
        cur_tdc_info.success_send_groups - last_tdc_info.success_send_groups,
        cur_tdc_info.failed_send_groups - last_tdc_info.failed_send_groups,
        cur_tdc_info.empty_list_length,
        cur_tdc_info.full_list_length,
        cur_tdc_info.sending_list_length);
#endif
    last_tdc_info = cur_tdc_info;
    last_dump_time_ms = cur_time_ms;
  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  delay_init(systemclock);
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_DAC1_Init();
  MX_TIM3_Init();
  MX_USART3_UART_Init();
  MX_TIM16_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM1_Init();
  MX_USB_PCD_Init();
  /* USER CODE BEGIN 2 */
  init_uart_driver();
  init_dac();
  Tdc_Driver_Init();     //MS1005
  init_apd_pwm();
  tdc_mgr_init();
  init_encoder_mgr();
  init_laser_pulse();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    tdc_mgr_send_data_once();
    dump_debug_info();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV4;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
