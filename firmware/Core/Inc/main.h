/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define APD_FB_Pin GPIO_PIN_1
#define APD_FB_GPIO_Port GPIOA
#define DAC_COMP1_Pin GPIO_PIN_4
#define DAC_COMP1_GPIO_Port GPIOA
#define DAC_COMP2_Pin GPIO_PIN_5
#define DAC_COMP2_GPIO_Port GPIOA
#define PULSE_START_Pin GPIO_PIN_6
#define PULSE_START_GPIO_Port GPIOA
#define MOTOR_PWM_Pin GPIO_PIN_0
#define MOTOR_PWM_GPIO_Port GPIOB
#define ENCODER_EXTI_Pin GPIO_PIN_1
#define ENCODER_EXTI_GPIO_Port GPIOB
#define ENCODER_EXTI_EXTI_IRQn EXTI1_IRQn
#define TDC2_SPI_NSS_Pin GPIO_PIN_2
#define TDC2_SPI_NSS_GPIO_Port GPIOB
#define TDC1_SPI_NSS_Pin GPIO_PIN_12
#define TDC1_SPI_NSS_GPIO_Port GPIOB
#define TDC_SPI_SCK_Pin GPIO_PIN_13
#define TDC_SPI_SCK_GPIO_Port GPIOB
#define TDC_SPI_MISO_Pin GPIO_PIN_14
#define TDC_SPI_MISO_GPIO_Port GPIOB
#define TDC_SPI_MOSI_Pin GPIO_PIN_15
#define TDC_SPI_MOSI_GPIO_Port GPIOB
#define TDC_RST_Pin GPIO_PIN_6
#define TDC_RST_GPIO_Port GPIOC
#define TDC1_INT_Pin GPIO_PIN_8
#define TDC1_INT_GPIO_Port GPIOA
#define TDC1_INT_EXTI_IRQn EXTI9_5_IRQn
#define TDC2_INT_Pin GPIO_PIN_9
#define TDC2_INT_GPIO_Port GPIOA
#define TDC2_INT_EXTI_IRQn EXTI9_5_IRQn
#define APD_PWM_Pin GPIO_PIN_10
#define APD_PWM_GPIO_Port GPIOA
#define ETH_SCK_Pin GPIO_PIN_10
#define ETH_SCK_GPIO_Port GPIOC
#define ETH_MISO_Pin GPIO_PIN_11
#define ETH_MISO_GPIO_Port GPIOC
#define ETH_INTN_Pin GPIO_PIN_3
#define ETH_INTN_GPIO_Port GPIOB
#define ETH_RSTN_Pin GPIO_PIN_4
#define ETH_RSTN_GPIO_Port GPIOB
#define ETH_MOSI_Pin GPIO_PIN_5
#define ETH_MOSI_GPIO_Port GPIOB
#define ETH_CS_Pin GPIO_PIN_6
#define ETH_CS_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
